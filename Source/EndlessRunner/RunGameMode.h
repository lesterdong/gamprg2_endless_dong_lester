// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNER_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()

	ARunGameMode();

protected: 
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class ATile> TileRef;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FVector SpawnPoint;

	UFUNCTION() 
		void CreateTile(bool spawnSpawnables);
	UFUNCTION()
		void DestroyTile(ATile * ToDestroy);
};
