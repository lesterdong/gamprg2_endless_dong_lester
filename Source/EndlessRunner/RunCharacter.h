// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RunCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDeathSignature, class ARunCharacter*, RunCharacter);

UCLASS()
class ENDLESSRUNNER_API ARunCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ARunCharacter();

	UPROPERTY(BlueprintAssignable)
		FOnDeathSignature OnDeath;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool IsAlive;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int32 Coins;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void Die();
	
	bool GetIsAlive();

	UFUNCTION(BlueprintCallable)
	int32 GetCoins();

	UFUNCTION(BlueprintCallable)
	void AddCoins(int32 add);

};
