// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "Math/Vector.h"
#include "Tile.h"
#include "Components/ArrowComponent.h"
#include "Engine/World.h"
#include "Delegates/DelegateSignatureImpl.inl"

ARunGameMode::ARunGameMode()
{
	SpawnPoint = FVector(-400, 0, 10);
}

void ARunGameMode::BeginPlay() {
	Super::BeginPlay();

	CreateTile(false);

	for (int i = 0; i < 5; i++) {
		CreateTile(true);
	}
}

void ARunGameMode::CreateTile(bool spawnSpawnables) {
	FActorSpawnParameters SpawnParameters;
	if (TileRef != nullptr) {
		ATile* TileActor = GetWorld()->SpawnActor<ATile>(TileRef, SpawnPoint, FRotator(0), SpawnParameters);
		SpawnPoint = FVector(TileActor->GetAttachPoint()->GetComponentLocation().X - 400.0f, 0, 10);
		TileActor->OnTileExited.AddDynamic(this, &ARunGameMode::DestroyTile);

		if (spawnSpawnables) {
			TileActor->SpawnSpawnables();
		}
	}

	
	//TileActor->OnTileExited.AddDynamic(this, &ARunGameMode::CreateTile);
	//TileActor->OnTileExited.AddDynamic(this, [](ATile* tile)-> void {CreateTile();})
	//TileActor->OnTileExited.BindLambda([] {CreateTile(); });
}

void ARunGameMode::DestroyTile(ATile* ToDestroy) {
	FTimerHandle Timer;
	FTimerDelegate TimerDelegate;
	
	TimerDelegate.BindLambda([ToDestroy] {
		ToDestroy->Destroy();
	});
	GetWorldTimerManager().SetTimer(Timer, TimerDelegate, 3.0f, false);
	CreateTile(true);
}

