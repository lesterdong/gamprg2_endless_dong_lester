// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"



UCLASS()
class ENDLESSRUNNER_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()
	
protected:

	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
	class ARunCharacter* ControlledCharacter;

	virtual void SetupInputComponent() override;
	void MoveRight(float scale);

public:
	virtual void Tick(float DeltaTime) override;

};
