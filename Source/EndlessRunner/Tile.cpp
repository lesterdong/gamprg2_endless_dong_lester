// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"
#include "RunCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Obstacle.h"
#include "Engine/EngineTypes.h"
#include "Pickup.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	SetRootComponent(Scene);

	AttachPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("Attach Point"));
	AttachPoint->SetupAttachment(RootComponent);

	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	Box->SetupAttachment(RootComponent);
	Box->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnBeginOverlap);

	ObstacleSpawnPoint = CreateDefaultSubobject<UBoxComponent>(TEXT("Obstacle Spawn Point"));
	ObstacleSpawnPoint->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATile::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* Character = Cast<ARunCharacter>(OtherActor)) {
		OnTileExited.Broadcast(this);
	}

}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

UArrowComponent* ATile::GetAttachPoint() {
	return AttachPoint;
}

void ATile::DestroyActor()
{
	if (this != nullptr) {
		Destroy();
	}
	
}

void ATile::SpawnSpawnables() {
	FVector SpawnPosition;

	if (ObstacleReference.Num() >= 1 && FMath::RandRange(0, 100) > 40) {
		SpawnPosition = UKismetMathLibrary::RandomPointInBoundingBox(ObstacleSpawnPoint->GetComponentLocation(), ObstacleSpawnPoint->GetScaledBoxExtent());
		AObstacle* ToSpawnObject = GetWorld()->SpawnActor<AObstacle>(ObstacleReference[FMath::RandRange(0, ObstacleReference.Num() - 1)], SpawnPosition, FRotator(0), FActorSpawnParameters());
		ToSpawnObject->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepWorld, true), ATile::GetAttachParentSocketName());
		OnDestroyed.AddDynamic(ToSpawnObject, &AObstacle::DestroyActor);
	}

	if (PickUpReference.Num() >= 1 && FMath::RandRange(0, 100) > 60) {
		SpawnPosition = UKismetMathLibrary::RandomPointInBoundingBox(ObstacleSpawnPoint->GetComponentLocation(), ObstacleSpawnPoint->GetScaledBoxExtent());
		APickup* ToSpawnObject = GetWorld()->SpawnActor<APickup>(PickUpReference[FMath::RandRange(0, PickUpReference.Num() - 1)], SpawnPosition, FRotator(0), FActorSpawnParameters());
		ToSpawnObject->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepWorld, true), ATile::GetAttachParentSocketName());
		OnDestroyed.AddDynamic(ToSpawnObject, &APickup::DestroyActor);
	}
}


