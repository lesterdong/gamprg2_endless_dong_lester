// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "RunCharacter.h"
#include "Kismet/GameplayStatics.h"

void ARunCharacterController::BeginPlay() {
	Super::BeginPlay();
	
	ControlledCharacter = Cast<ARunCharacter, APawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	Possess(ControlledCharacter);
}

void ARunCharacterController::MoveRight(float scale) {
	ControlledCharacter->AddMovementInput(ControlledCharacter->GetActorRightVector(), scale);
}

void ARunCharacterController::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	ControlledCharacter->AddMovementInput(ControlledCharacter->GetActorForwardVector(), 1);
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("Horizontal", this, &ARunCharacterController::MoveRight);
}
